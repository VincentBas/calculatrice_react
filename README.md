## Installation

### Clone this repository

`git clone [nom_repo]`

### Launch the app

`yarn && yarn start`

## Choix

-   Jest pour les tests unitaires
-   CDN Bootstrap pour le design
-   redux-thunk pour les actions paramétrables
-   Babel pour transpiler mon Javascript
-   linter standard-react
-   SCSS

## Fonctionnement

Ma calculatrice ne gère pas les priorités de calcul<br>
Un calcul est consitué de trois variables:<br>

-   (_result_) => résultat de l'opération. Il s'agit d'un nombre
-   (_input_) => entrée de l'utilisateur que ce soit un nombre ou un opérateur. Il s'agit d'un string
-   (_operation_) => les différentes opération. Il s'agit d'un objet composé d'un string et d'une fonction<br>

Les tests unitaires permettent de couvrir de nombreux cas.<br>

-   On ne peut pas écrire 0.0.0 ou 5+-.
-   Idem on peut pas diviser par 0
-   ...

Mode Aléatoire<br>

-   En mode développeur lorsqu'on appuie sur espace dans l'input, on construit aléatoirement les différentes opérations.
