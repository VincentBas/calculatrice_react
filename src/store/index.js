import { combineReducers } from 'redux'
import { role } from './reducer/role'
import { calculator } from './reducer/calculator'

export default combineReducers({
    role,
    calculator
})
