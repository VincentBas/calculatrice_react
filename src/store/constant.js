export const RESULT_ERROR = 'error'

export const operations = {
    default: { display: '', operation: (a, b) => a + b },
    add: { display: '+', operation: (a, b) => a + b },
    substract: { display: '-', operation: (a, b) => a - b },
    multiply: { display: 'x', operation: (a, b) => a * b },
    divide: { display: '/', operation: (a, b) => a / b }
}
