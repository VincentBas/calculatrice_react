import { operations, RESULT_ERROR } from './../constant'

const initialState = {
    result: 0,
    input: '',
    operation: operations.default
}

/**
 * @desc display operation
 * @return string
 **/
export function displayCalculatorLabel({ result, input, operation }) {
    return (operation === operations.default && result === 0) || 
    result === RESULT_ERROR ? `${input}` : `${result} ${operation.display} ${input}`
}

/**
 * @desc Concatenate value of input
 * @return string
 **/
export function concatValues(stateValue, value) {
    const newValue = `${stateValue}${value}`
    const isNumber = !isNaN(Number(newValue))
    if (newValue === '-') return newValue
    return isNumber ? newValue : stateValue
}

/**
 * @desc return operation result
 * @return number
 **/
export function computeResult({ result, input, operation }) {
    const nextResult = operation.operation(result, Number(input))
    return input !== '' ? (isFinite(nextResult) ? nextResult : RESULT_ERROR) : result
}

/**
 * @desc get operation object
 * @return object
 **/
export function getOperation(operationInput) {
    return operations[operationInput] ? operations[operationInput] : operations.default
}

export const calculator = (state = initialState, action) => {
    const { input } = state
    const { type, value } = action
    switch (type) {
        case 'CLEAR':
            return initialState
        case 'COMPUTE':
            return {
                ...initialState,
                result: computeResult(state)
            }
        case 'INPUT':
            return { ...state, input: concatValues(input, value) }
        case 'OPERATION':
            return { ...state, operation: getOperation(value) }
        default:
            return state
    }
}
