import { operations } from './constant'

export const clearAction = () => ({ type: 'CLEAR' })

export const computeResultAction = () => ({ type: 'COMPUTE' })

export const addInputAction = value => ({ type: 'INPUT', value })

export const updateOperationAction = op => ({ type: 'OPERATION', value: op })

export function addInput(value) {
    return function(dispatch, getState) {
        const { calculator } = getState()
        const { result, operation } = calculator
        if (result !== 0 && operation === operations.default) {
            dispatch(clearAction())
            return dispatch(addInputAction(value))
        }
        return dispatch(addInputAction(value))
    }
}

export function clearCalculator() {
    return function(dispatch) {
        return dispatch(clearAction())
    }
}

export function compute() {
    return function(dispatch) {
        return dispatch(computeResultAction())
    }
}

export function updateOperation(op) {
    return function(dispatch) {
        dispatch(computeResultAction())
        return dispatch(updateOperationAction(op))
    }
}

function getRandomInt(min, max) {
    return Math.round(Math.random() * (max - min)) + min
}

export function randomComputation() {
    return function(dispatch, getState) {
        const { input } = getState().calculator
        const data = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', 'add', 'substract', 'multiply', 'divide']
        const randomInt = input === '' ? getRandomInt(1, data.length) : getRandomInt(0, data.length)
        const elem = data[randomInt]
        if (['add', 'substract', 'multiply', 'divide'].includes(elem)) {
            dispatch(computeResultAction())
            return dispatch(updateOperationAction(elem))
        }
        return dispatch(addInputAction(elem))
    }
}
