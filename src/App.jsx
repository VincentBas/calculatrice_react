import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Container from './components/Container'
import './assets/scss/main.scss'

const Routes = () => (
    <Router>
        <Switch>
            <Route exact path='/' component={Container} />
        </Switch>
    </Router>
)
export default Routes
