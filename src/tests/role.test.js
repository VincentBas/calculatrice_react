import { role, switchToUserRole, switchToDevRole, USER_ROLE, DEV_ROLE } from './../store/reducer/role'

describe('Role Reducer', () => {
    describe('action', () => {
        it('should update the role to user', () => {
            const state = { role: DEV_ROLE }
            expect(role(state, switchToUserRole).role).toBe(USER_ROLE)
        })
        it('should not change the role to user', () => {
            const state = { role: DEV_ROLE }
            expect(role(state, switchToDevRole).role).toBe(DEV_ROLE)
        })
        it('should update the role to dev', () => {
            const state = { role: USER_ROLE }
            expect(role(state, switchToDevRole).role).toBe(DEV_ROLE)
        })
    })
})
