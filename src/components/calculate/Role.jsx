import React, { Component } from 'react'
import { USER_ROLE, switchToUserRole, switchToDevRole } from './../../store/reducer/role'
import { connect } from 'react-redux'

const mapStateToProps = ({ role }) => ({
    role: role.role
})

const mapDispatchToProps = dispatch => ({
    toDev: () => {
        dispatch(switchToDevRole)
    },
    toUser: () => {
        dispatch(switchToUserRole)
    }
})

class RoleComponent extends Component {
    render() {
        const { role, toDev, toUser } = this.props
        const isNormalUser = role === USER_ROLE
        return (
            <div className='btn-group'>
                <label className='btn btn-primary'>
                    <input type='radio' name='options' id='option1' onChange={() => this.props.toUser()} /> User
                </label>
                <label className='btn btn-primary'>
                    <input type='radio' name='options' id='option2' onChange={() => this.props.toDev()} /> Dev
                </label>
            </div>
        )
    }
}

export const Role = connect(
    mapStateToProps,
    mapDispatchToProps
)(RoleComponent)
