import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { displayCalculatorLabel } from './../../store/reducer/calculator'
import { randomComputation } from './../../store/action'
import { DEV_ROLE } from './../../store/reducer/role'
import { connect } from 'react-redux'

const mapStateToProps = ({ calculator, role }) => ({
    role: role.role,
    result: calculator.result,
    currentOps: displayCalculatorLabel(calculator)
})

const mapDispatchToProps = dispatch => bindActionCreators({ randomComputation }, dispatch)

class LabelsComponent extends Component {
    handlePress = e => {
        console.log(this.props.role)
        if (e.key === ' ' && this.props.role === DEV_ROLE) {
            this.props.randomComputation()
        }
    }

    render() {
        const { result, currentOps } = this.props
        return (
            <div className='form-group'>
                <label htmlFor='currentOps'>{result}</label>
                <input id='currentOps' readOnly className='form-control' onKeyPress={this.handlePress} value={currentOps} />
            </div>
        )
    }
}

export const Labels = connect(
    mapStateToProps,
    mapDispatchToProps
)(LabelsComponent)
