import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { addInput, clearCalculator, compute, updateOperation } from './../../store/action'

const mapDispatchToProps = dispatch => bindActionCreators({ addInput, clearCalculator, compute, updateOperation }, dispatch)

class GridComponent extends Component {
    renderButtons() {
        const { addInput, compute } = this.props
        const buttonData = [
            { label: '1', onClick: () => addInput('1'), color: 'default' },
            { label: '2', onClick: () => addInput('2'), color: 'default' },
            { label: '3', onClick: () => addInput('3'), color: 'default' },
            { label: '4', onClick: () => addInput('4'), color: 'default' },
            { label: '5', onClick: () => addInput('5'), color: 'default' },
            { label: '6', onClick: () => addInput('6'), color: 'default' },
            { label: '7', onClick: () => addInput('7'), color: 'default' },
            { label: '8', onClick: () => addInput('8'), color: 'default' },
            { label: '9', onClick: () => addInput('9'), color: 'default' },
            { label: '0', onClick: () => addInput('0'), color: 'default' },
            { label: '.', onClick: () => addInput('.'), color: 'default' },
            { label: '=', onClick: () => compute(), color: 'success' }
        ]
        return buttonData.map(({ label, onClick, color }) => (
            <span className='col-4'>
                <button key={label} className={`button__margin numbers btn-${color}`} onClick={onClick} value={label}>
                    {label}
                </button>
            </span>
        ))
    }

    renderOperators() {
        const { clearCalculator, updateOperation } = this.props
        const buttonData = [
            {
                label: '/',
                onClick: () => updateOperation('divide'),
                color: 'warning'
            },
            {
                label: '*',
                onClick: () => updateOperation('multiply'),
                color: 'warning'
            },
            {
                label: '-',
                onClick: () => updateOperation('substract'),
                color: 'warning'
            },
            {
                label: '+',
                onClick: () => updateOperation('add'),
                color: 'warning'
            },
            { label: 'C', onClick: () => clearCalculator(), color: 'warning' }
        ]
        return buttonData.map(({ label, onClick, color }) => (
            <div className='row' key={label}>
                <span className='col-12 '>
                    <button className={`button__margin operators btn-${color}`} onClick={onClick} value={label}>
                        {label}
                    </button>
                </span>
            </div>
        ))
    }

    render() {
        return (
            <div className='row'>
                <div className='col-9 no__padding'>
                    <div className='row'>{this.renderButtons()}</div>
                </div>
                <div className='col-3 no__padding'>{this.renderOperators()}</div>
            </div>
        )
    }
}

export const Grid = connect(
    undefined,
    mapDispatchToProps
)(GridComponent)
