import React, { Component } from 'react'
import { Grid } from './calculate/Grid.jsx'
import { Labels } from './calculate/Labels.jsx'
import { Role } from './calculate/Role.jsx'
class Container extends Component {
    render() {
        return (
            <div className='app'>
                <div className='container'>
                    <div className='cr__center'>
                        <Role />
                    </div>
                    <div className='cr__card'>
                        <Labels />
                        <Grid />
                    </div>
                </div>
            </div>
        )
    }
}

export default Container
